﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.Extensions.Configuration;
using Nest;
using Xunit;

namespace Bookatable.Logstash.IndexCleaner.Tests
{
    public class CleanOldLogstashIndicesTests : IAsyncLifetime
    {
        private List<Tuple<string, bool>> _indices;
        private readonly int _indexIsOldIfOlderThan;
        private readonly IConfigurationRoot _configuration;
        private readonly string _environmentName;
        private readonly string _indexFormat;

        public CleanOldLogstashIndicesTests()
        {

            var environmentConfig = new ConfigurationBuilder().AddEnvironmentVariables().Build();
            _environmentName = environmentConfig.GetValue<string>("NETCORE_ENVIRONMENT");

            Console.WriteLine($"Environment is {_environmentName}");

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile($"appsettings.{_environmentName}.json", optional: true)
                .AddEnvironmentVariables().Build();

            _indexFormat = _configuration.GetValue<string>("ElasticSearch:LogstashIndexFormat");
            _indexIsOldIfOlderThan = _configuration.GetValue<int>("ElasticSearch:DeleteIndexIfOlderThan");
        }

        [Fact]
        public async Task Handle_WhenCalled_DeletesOldIndices()
        {
            
            await CreateIndices();

            var invokeResponse =  await TriggerLambda();

            Assert.Equal(200, invokeResponse.StatusCode);

            var client = CreateClient();

            foreach (var index in _indices.Where(x => x.Item2))
            {
                var result = await client.GetIndexAsync(index.Item1);
                Assert.False(result.IsValid);
            }

            foreach (var index in _indices.Where(x => !x.Item2))
            {
                var result = await client.GetIndexAsync(index.Item1);
                Assert.True(result.IsValid);
            }
        }

        private async Task<InvokeResponse> TriggerLambda()
        {
            using(var client = new AmazonLambdaClient(new AmazonLambdaConfig
            {
                RegionEndpoint = RegionEndpoint.EUWest1
            }))
            {
                return await client.InvokeAsync(new InvokeRequest
                {
                    FunctionName = _configuration.GetValue<string>("LambdaFunctionName"),
                    InvocationType = InvocationType.RequestResponse
                });

                
            }
        }

        private async Task CreateIndices()
        {
            var client = CreateClient();

            _indices = new List<Tuple<string, bool>>();

            //Create 2 new and 3 old indices
            var startDay = _indexIsOldIfOlderThan - 2;

            var now = DateTime.Now;
            for (var i = 0; i < 5; i++)
            {
                var oldDay = now.AddDays(-startDay - i).Date;

                var indexName = string.Format(_indexFormat, oldDay.Year, oldDay.Month, oldDay.Day);
                var result = await client.CreateIndexAsync(new IndexName { Name = indexName });
                
                _indices.Add(new Tuple<string, bool>(indexName, oldDay.AddDays(_indexIsOldIfOlderThan) < now));
            }

            var randomIndexName = Guid.NewGuid().ToString();
            await client.CreateIndexAsync(new IndexName { Name = randomIndexName });
            _indices.Add(new Tuple<string, bool>(randomIndexName, false));
        }

        private ElasticClient CreateClient()
        {
            var elasticSearchUrl = _configuration.GetValue<string>("ElasticSearch:Url");
            var region = _configuration.GetValue<string>("ElasticSearch:Region");

            if (_environmentName == null)
            {
                var userName = _configuration.GetValue<string>("ElasticSearch:UserName");
                var password = _configuration.GetValue<string>("ElasticSearch:Password");

                using (var pool = new SingleNodeConnectionPool(new Uri(elasticSearchUrl)))
                using (var settings = new ConnectionSettings(pool).BasicAuthentication(userName, password))
                {
                    return new ElasticClient(settings);
                }
            }

            var httpConnection = new AwsHttpConnection(region, new CredentialChainProvider());
            using (var pool = new SingleNodeConnectionPool(new Uri(elasticSearchUrl)))
            using (var settings = new ConnectionSettings(pool, httpConnection))
            {
                return new ElasticClient(settings);
            }

        }

        public Task InitializeAsync()
        {
            return Task.CompletedTask;
        }

        public async Task DisposeAsync()
        {
            foreach (var index in _indices)
            {
                var client = CreateClient();
                var result = await client.DeleteIndexAsync(new DeleteIndexRequest(index.Item1));

                Console.WriteLine(result.IsValid);
            }
        }
    }
}
