﻿namespace Bookatable.Logstash.IndexCleaner
{
    public enum ElasticSearchAuthenticationType
    {
        Basic,
        AWS_IAM
    }
}