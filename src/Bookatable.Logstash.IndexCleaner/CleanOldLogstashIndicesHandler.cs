using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Elasticsearch.Net;
using Elasticsearch.Net.Aws;
using Microsoft.Extensions.Configuration;
using Nest;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Bookatable.Logstash.IndexCleaner
{
    public class CleanOldLogstashIndicesHandler
    {
        private readonly string _environmentName;
        private readonly IConfigurationRoot _configuration;
        private readonly int _deleteIndexIfOlderThan;

        public CleanOldLogstashIndicesHandler()
        {
            _environmentName = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");

            Console.WriteLine($"Environment is {_environmentName}");

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile($"appsettings.{_environmentName}.json", optional: true)
                .AddEnvironmentVariables().Build();

            _deleteIndexIfOlderThan = _configuration.GetValue<int>("ElasticSearch:DeleteIndexIfOlderThan");
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task HandleAsync(ILambdaContext context)
        {
            try
            {
                if (!_configuration.GetValue<bool>("ElasticSearch:Enabled"))
                {
                    context.Logger.Log("Function is not enabled. Exiting.");
                }

                context.Logger.Log("Starting to clear old indices on logstash");

                var client = CrateElasticClient(context.Logger);
                context.Logger.Log("Client has been created");
                var indices = await client.CatIndicesAsync(new CatIndicesRequest("logstash-*"));

                if (!indices.IsValid)
                {
                    context.Logger.Log(
                        $"Request to ElasticSearch to retrieve the indices was not valid. Reason: {indices?.ServerError?.Error?.Reason}");

                    return;
                }

                context.Logger.Log($"Found {indices.Records.Count} indices with the prefix logstash-*");

                foreach (var indexRecord in indices.Records)
                {
                    var numbers = indexRecord.Index.Split('-')[1].Split('.').Select(int.Parse).ToList();
                    var date = new DateTime(numbers[0], numbers[1], numbers[2]);

                    if (date.AddDays(_deleteIndexIfOlderThan) < DateTime.Now)
                    {
                        context.Logger.Log($"Index {indexRecord.Index} is old. Will be deleted");

                        await client.DeleteIndexAsync(indexRecord.Index);

                    }
                    else
                    {
                        context.Logger.Log($"Index {indexRecord.Index} is not old.");
                    }
                }
            }
            catch (Exception e)
            {
                context.Logger.Log($"Exception occured. {e.Message}, {e.StackTrace}");
                throw;
            }
        }

        private ElasticClient CrateElasticClient(ILambdaLogger logger)
        {
            var elasticSearchUrl = _configuration.GetValue<string>("ElasticSearch:Url");
            var region = _configuration.GetValue<string>("ElasticSearch:Region");
            var authType = _configuration.GetValue<ElasticSearchAuthenticationType>("ElasticSearch:AuthenticationType");

            logger.Log($"ElasticSearch URL is: {elasticSearchUrl} and the region is {region}");

            switch (authType)
            {
                case ElasticSearchAuthenticationType.Basic:
                    logger.Log($"Auth type is Basic");
                    var userName = _configuration.GetValue<string>("ElasticSearch:UserName");
                    var password = _configuration.GetValue<string>("ElasticSearch:Password");

                    using (var pool = new SingleNodeConnectionPool(new Uri(elasticSearchUrl)))
                    using (var settings = new ConnectionSettings(pool).BasicAuthentication(userName, password))
                    {
                        return new ElasticClient(settings);
                    }
                case ElasticSearchAuthenticationType.AWS_IAM:
                    logger.Log($"Auth type is IAM");
                    var httpConnection = new AwsHttpConnection(region, new CredentialChainProvider(new List<ICredentialsProvider>
                    {
                        new EnvironmentVariableCredentialsProvider(),
                        new InstanceProfileCredentialProvider()
                    }));
                    using (var pool = new SingleNodeConnectionPool(new Uri(elasticSearchUrl)))
                    using (var settings = new ConnectionSettings(pool, httpConnection))
                    {
                        return new ElasticClient(settings);
                    }
                default:
                    throw new ArgumentOutOfRangeException($"ElasticSearchAuthenticationType");
            }
        }
    }
}
